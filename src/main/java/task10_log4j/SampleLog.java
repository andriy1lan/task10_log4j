package task10_log4j;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SampleLog {

    private static Logger logger = LogManager.getLogger();
    public static void main(String [] args){
  
        logger.debug("This is a debug message - main started");
        logger.info("This is an info message");
        logger.warn("This is a warn message");
        try{
        	throw new Exception("error message");
        }
        catch (Exception e) { logger.error(e.getMessage() + " was caught");}
        logger.error("This is a error message");
        logger.fatal("This is a fatal message");
    }
}